import { resetUsername } from './actions.mjs';

export default socket => {
  socket.on('RESET_USERNAME', resetUsername);
};
