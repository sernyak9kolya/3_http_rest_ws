import handleUsername from './username/index.mjs';
import handleRooms from './rooms/index.mjs';
import handleGame from './game/index.mjs';

const username = sessionStorage.getItem("username");
document.querySelector('#username').innerHTML = username;

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

handleUsername(socket);
handleRooms(socket);
handleGame(socket);
